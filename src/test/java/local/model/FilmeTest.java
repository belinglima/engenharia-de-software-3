package local.model;

import org.hamcrest.CoreMatchers;
import static org.junit.Assume.assumeTrue;
import org.junit.Assert;
import org.junit.Test;

import local.exception.FilmeSemEstoqueException;
import local.exception.LocadoraException;
import local.service.LocacaoService;
import local.util.DataUtils;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

public class FilmeTest {

	@Test
	public void locarTresFilmesDeveTerUmQuartoDeDesconto() throws FilmeSemEstoqueException, LocadoraException{
		Locacao locacao = new Locacao();
		Filme filme1 = new Filme();
		filme1.setEstoque(100);
		filme1.setNome("teste nome filme");
		filme1.setPrecoLocacao(3.00);
		Cliente cliente = new Cliente();
		cliente.setNome("Angelo Luz");
		List<Filme> arrayFilme = new ArrayList();
		for(int i=0;i<3; i++){
			arrayFilme.add(filme1);
		}
		
		LocacaoService ls = new LocacaoService();
		locacao = ls.alugarFilme(cliente, arrayFilme);
		Double valor = locacao.getValor();
		System.out.println("preço: "+locacao.getValor());
		assertEquals(valor, 8.25, 0.001);
	}
	
	@Test
	public void locarQuatroFIlmesDeveterMetadeDesconto() throws FilmeSemEstoqueException, LocadoraException{
		Locacao locacao = new Locacao();
		Filme filme1 = new Filme();
		filme1.setEstoque(100);
		filme1.setNome("teste nome filme");
		filme1.setPrecoLocacao(3.00);
		Cliente cliente = new Cliente();
		cliente.setNome("Angelo Luz");
		List<Filme> arrayFilme = new ArrayList();
		for(int i=0;i<4; i++){
			arrayFilme.add(filme1);
		}
		
		LocacaoService ls = new LocacaoService();
		locacao = ls.alugarFilme(cliente, arrayFilme);
		Double valor = locacao.getValor();
		System.out.println("preço: "+locacao.getValor());
		assertEquals(valor, 10.5, 0.001);
	}
	
	@Test
	public void locarCincoOuMaisDeveterTodoDesconto() throws FilmeSemEstoqueException, LocadoraException{
		Locacao locacao = new Locacao();
		Filme filme1 = new Filme();
		filme1.setEstoque(100);
		filme1.setNome("teste nome filme");
		filme1.setPrecoLocacao(3.00);
		Cliente cliente = new Cliente();
		cliente.setNome("Angelo Luz");
		List<Filme> arrayFilme = new ArrayList();
		for(int i=0;i<5; i++){
			arrayFilme.add(filme1);
		}
		
		LocacaoService ls = new LocacaoService();
		locacao = ls.alugarFilme(cliente, arrayFilme);
		Double valor = locacao.getValor();
		System.out.println("preço: "+locacao.getValor());
		assertEquals(valor, 12.0, 0.001);
	}
	
	@Test
	public void seLocarNoSabadoEntregaNaSegunda(){
		Date dataDeLocacao = new Date();
		Date dataDeEntrega = new Date();
		assumeTrue(DataUtils.verificarDiaSemana(dataDeLocacao, Calendar.SATURDAY));
		String data="";
		if(DataUtils.verificarDiaSemana(dataDeLocacao, Calendar.SATURDAY)){
			dataDeEntrega = DataUtils.adicionarDias(dataDeLocacao, 2);
			data = dataDeEntrega.toString();
		}else{
			dataDeEntrega = DataUtils.adicionarDias(dataDeLocacao, 1);
			data = dataDeEntrega.toString();
		}
		System.out.println("dia da semana: "+dataDeEntrega);
		
		if(data.contains("Sun")){
			fail("Data de Entrega não pode ser domingo!");
		}
		assertTrue(data.contains("Mon"));
		assertFalse(DataUtils.isMesmaData(dataDeLocacao, dataDeEntrega));
		
	}

}
