package local.suites;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import local.model.ClienteTest;
import local.model.FilmeTest;
import local.service.LocacaoServiceTest;


@RunWith(Suite.class)
@SuiteClasses({ClienteTest.class, FilmeTest.class, LocacaoServiceTest.class})
public class AllTests {

	@BeforeClass
	public static void setUp(){
			
	}
		
	@AfterClass
	public static void afterAll(){
			
	}
	
	
}
